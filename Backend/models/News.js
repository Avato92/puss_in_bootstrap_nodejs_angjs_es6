var mongoose = require('mongoose');

var NewSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: String,
    author: String,
    category: String,
    location: {
        lat: String,
        lon: String
    },
    date: String,
    sumary: String,
    body: Array,
    img: String,
    bodyImg: Array
});

mongoose.model('News', NewSchema);