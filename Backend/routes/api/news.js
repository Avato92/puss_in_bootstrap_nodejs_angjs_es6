var router = require('express').Router();
var mongoose = require('mongoose');
var News = mongoose.model('News');

router.get('/', function(req, res, next){
     News.find().then(function(news){
      return res.json({news:news});
    }).catch(next); 
});
router.get('/:id', function(req, res, next) {
  News.findById(req.params.id).then(function(news){
    if(!news){ 
      return res.sendStatus(401); 
    }
    return res.json({news: news});
  }).catch(next);
});

module.exports = router;