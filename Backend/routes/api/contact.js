var router = require('express').Router();
var email = require('../../utils/email.js');

router.post('/', function(req, res, next) {
  email.sendEmail(req,res);
  return true;
});

module.exports = router;