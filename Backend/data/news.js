db.news.insert({
    title: "Prueba",
    author: "Avato",
    category:"International",
    location:{
        lat:"39.4646856",
        lon:"-0.3860902"
    }, 
    date : new Date(),
    summary: "bla bla bla", 
    body: ["bla bla bla"],
    img: "images/about-img.jpg",
    bodyImg: ["images/about-img.jpg"]
});

db.news.insert({
    title: "Prueba2", 
    author: "Avato", 
    category:"National", 
    location:{
        lat:"39.4746856", 
        lon:"-0.3960902"
    }, 
    date : new Date(),
    summary: "bla bla bla", 
    body: ["bla bla bla"],
    img: "images/about-img.jpg",
    bodyImg: ["images/about-img.jpg"]
});

db.news.insert({
    title: "Prueba3", 
    author: "Avato", 
    category:"Culture", 
    location:{
        lat:"39.4946856", 
        lon:"-0.4060902"
    }, 
    date : new Date(),
    summary: "bla bla bla", 
    body: ["bla bla bla"],
    img: "images/about-img.jpg",
    bodyImg: ["images/about-img.jpg"]
});

db.news.insert({
    title: "Prueba4", 
    author: "Avato", 
    category:"Sport", 
    location:{
        lat:"39.5046856", 
        lon:"-0.4160902"
    }, 
    date : new Date(),
    summary: "bla bla bla", 
    body: ["bla bla bla"],
    img: "images/about-img.jpg",
    bodyImg: ["images/about-img.jpg"]
});

db.news.insert({
    title: "Prueba5",
    author: "Avato",
    category:"International",
    location:{
        lat:"39.5146856",
        lon:"-0.4260902"
    }, 
    date : new Date(),
    summary: "bla bla bla", 
    body: ["bla bla bla", "bla2 bla2 bla2", "bla3 bla3 bla3"],
    img: "images/about-img.jpg",
    bodyImg: ["images/about-img.jpg", "images/about-img.jpg", "images/about-img.jpg"]
});