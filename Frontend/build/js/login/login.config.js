function LoginConfig($stateProvider/* , User */) {
    'ngInject';
  
    $stateProvider

    .state('app.login', {
      url: '/login',
      controller: 'LoginCtrl as $ctrl',
      templateUrl: 'login/login.html',
      title: 'Sign in',
    })
  
    .state('app.register', {
      url: '/register',
      controller: 'LoginCtrl as $ctrl',
      templateUrl: 'login/login.html',
      title: 'Sign up',
    })
    .state('app.sociallogin', {
      url: '/login/sociallogin',
      controller: 'SocialCtrl as $ctrl',
      title: 'Sign up',
/*       resolve: {
        auth: function(User) {
          console.log(User);
          return User.ensureAuthIs(false);
        }
      } */
    });
  };
  
  export default LoginConfig;