import angular from 'angular';

// Create the module where our functionality can attach to
let loginModule = angular.module('app.login', []);

// Include our UI-Router config settings
import LoginConfig from './login.config';
loginModule.config(LoginConfig);


// Controllers
import LoginCtrl from './login.controller';
loginModule.controller('LoginCtrl', LoginCtrl);

import SocialCtrl from './social.controller';
loginModule.controller('SocialCtrl', SocialCtrl);


export default loginModule;