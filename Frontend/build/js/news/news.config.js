function NewsConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.news', {
      url: '/news',
      controller: 'NewsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'news/news.html',
      title: 'News',
       resolve:{
        news: function(News, $state){
          return News.getNews().then(
            (News) => News,
            (err) => $state.go('app.home')
          )
        }
      } 
    })
    .state('app.detailNew', {
      url: '/news/:id',
      controller: 'DetailNewCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'news/detailNew.html',
      title: 'Detail New',
      resolve: {
        news: function(News, $state, $stateParams) { 
          return News.getNew($stateParams.id).then(
            (News) => News,
            (err) => $state.go('app.home')
          )
        }
      }  
    });
  
  };
  
  export default NewsConfig;