import angular from 'angular';

// Create the module where our functionality can attach to
let newsModule = angular.module('app.news', []);

// Include our UI-Router config settings
import NewsConfig from './news.config';
newsModule.config(NewsConfig);


// Controllers
import NewsCtrl from './news.controller';
newsModule.controller('NewsCtrl', NewsCtrl);

import DetailNewCtrl from './detailNew.controller';
newsModule.controller('DetailNewCtrl', DetailNewCtrl);

export default newsModule;