export default class News{
    constructor(AppConstants, $http, $q){
        'ngInject'

        this._AppConstants = AppConstants;
        this._$http = $http;
    }

    getNews(){
        return this._$http({
            url: `${this._AppConstants.api}/news`,
            method: 'GET',
        }).then((res)=> res.data.news);
    }
    getNew(id){
        return this._$http({
            url: this._AppConstants.api + '/news/' + id,
            method: 'GET',
        }).then((res)=> res.data.news);
    }
}
