import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);

import ContactService from './contact.service';
servicesModule.service('Contact', ContactService);

import ToasterService from './toaster.service';
servicesModule.service('Toaster', ToasterService);

import NewsService from './news.service';
servicesModule.service('News', NewsService);

import JwtService from './jwt.service'
servicesModule.service('JWT', JwtService);

import LoginService from './login.service';
servicesModule.service('User', LoginService);

export default servicesModule;