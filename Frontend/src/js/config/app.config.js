/* import authInterceptor from './auth.interceptor' */

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider, toastrConfig) {
    'ngInject';
  
    $stateProvider
    .state('app', {
      abstract: true,
      templateUrl: 'layout/app-view.html'
    });
  
    angular.extend(toastrConfig, {
      autoDismiss: false,
      containerId: 'toast-container',
      maxOpened: 0,
      newestOnTop: true,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      preventOpenDuplicates: false,
      target: 'body'
    });
  
  
    $urlRouterProvider.otherwise('/');
  
  }
  
  export default AppConfig;
  