class NewsCtrl{
    constructor(AppConstants ,news){
        'ngInject';
        this.news = news;
        this.currentPage = 1;
        this.itemsPerPage = 3;
        this.filteredNews = [];

        let vm = this;

        vm.pageChanged = function() {
            update();
          };

          function update(){
            var begin = ((vm.currentPage - 1) * vm.itemsPerPage), end = begin + vm.itemsPerPage;
            vm.filteredNews = vm.news.slice(begin, end);
          }
          update();
    }
}

export default NewsCtrl;