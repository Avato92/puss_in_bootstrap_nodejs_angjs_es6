function LoginConfig($stateProvider) {
    'ngInject';
  
    $stateProvider

    .state('app.login', {
      url: '/login',
      controller: 'LoginCtrl as $ctrl',
      templateUrl: 'login/login.html',
      title: 'Sign in',
    })
  
    .state('app.register', {
      url: '/register',
      controller: 'LoginCtrl as $ctrl',
      templateUrl: 'login/login.html',
      title: 'Sign up',
    })
    .state('app.sociallogin', {
      url: '/login/sociallogin',
      controller: 'SocialCtrl as $ctrl',
      title: 'Sign up',
    });
  };
  
  export default LoginConfig;